<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20180306065323 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf(
            'mysql' !== $this->connection
                ->getDatabasePlatform()
                ->getName(),
            'Migration can only be executed safely on "mysql".'
        );

        $this->addSql('CREATE TABLE message (
            id INT AUTO_INCREMENT NOT NULL,
            sender VARCHAR(128) NOT NULL,
            message LONGTEXT NOT NULL,
            sentAt DATETIME NOT NULL,
            PRIMARY KEY(id)
        ) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf(
            'mysql' !== $this->connection
                ->getDatabasePlatform()
                ->getName(),
            'Migration can only be executed safely on "mysql".'
        );

        $this->addSql('DROP TABLE message');
    }
}
