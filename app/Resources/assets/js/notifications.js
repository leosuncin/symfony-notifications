import { connect } from 'js/sse';
import router from 'js/router';

const whoami = window.chat_msg_sender.value;

const handlers = {
  open() {
    console.log('Stream opened');
  },
  message(event) {
    const message = JSON.parse(event.data);
    if (message.sender === whoami) return
    const html = `<div class="message--image">
  <canvas data-jdenticon-value="${message.sender}" width="64" height="64">Identicon of ${message.sender}</canvas>
</div>
<div class="message--body">
  <strong class="message--title">${message.sender}</strong>
  <time class="message--time" datetime="${message.sentAt}">${message.sentAt}</time>
  <p class="message--text">${message.message.replace('\n', '<br>')}</p>
</div>`;
    const child = document.createElement('dd')
    child.classList.add('message');
    child.setAttribute('id', `message_${message.id}`);
    child.innerHTML = html;
    document.getElementById('message-list').appendChild(child);
  },
  user(event) {
    if (event.data === whoami) return
    alert(`${event.data} has joined.`);
  }
};

connect(router.generate('notification_sse'), handlers)
