export const connect = (url, handlers) => {
  const source = new EventSource(url);

  for (const event in handlers) {
    source.addEventListener(event, handlers[event]);
  }

  return source;
}

