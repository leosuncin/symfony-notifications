import 'styles';

import router from 'js/router';

if (location.pathname.endsWith(router.generate('chatpage'))) {
  import(
    /* webpackChunkName: "chat", webpackMode: "lazy" */
    'js/chat'
  )
}
