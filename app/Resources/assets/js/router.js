import routes from 'fos_js_routes.json';
import routing from 'FOSJsRouting';

routing.setRoutingData(routes);

export default routing;
