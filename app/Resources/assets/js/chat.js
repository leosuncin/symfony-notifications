import 'js/notifications';

document.forms.namedItem('chat_msg').addEventListener('submit', async event => {
  const $form = event.target;
  event.preventDefault();

  if (!$form.checkValidity()) return;

  const formData = new FormData($form);
  const newMessage = {};

  for (const [key, value] of formData.entries()) {
    const [, name] = /\[(.*)\]/.exec(key);
    if (name === '_token') continue;
    newMessage[name] = value;
  }

  const headers = new Headers({
    'Accept': 'application/json, text/plain, */*',
    'Content-Type': 'application/json'
  });

  try {
    const resp = await fetch($form.action, {
      method: 'POST',
      body: JSON.stringify(newMessage),
      headers
    });

    if (resp.ok) {
      $form.reset();
    }
  } catch (ex) {
    console.error(ex)
  }
});
