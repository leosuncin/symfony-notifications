FROM php:7.2-cli AS base
RUN apt-get update && apt-get install -yqq git libxml2-dev libzip-dev libfreetype6-dev libjpeg62-turbo-dev libpng-dev
RUN docker-php-ext-configure gd --with-freetype-dir=/usr/include/ --with-jpeg-dir=/usr/include/ &&\
  docker-php-ext-configure zip --with-libzip=/usr/include/ &&\
  docker-php-ext-install -j$(nproc) mbstring zip intl pdo pdo_mysql gd &&\
  pecl channel-update pecl.php.net && \
  pecl install apcu && \
  docker-php-ext-enable apcu
RUN printf '[APC]\napc.enable_cli = On\napc.enabled = On\napc.shm_size = 64M\n'\
  > /usr/local/etc/php/conf.d/apc.ini
RUN curl -sS https://getcomposer.org/installer | php -- --install-dir=/usr/local/bin --filename=composer


FROM base AS dependencies
ENV COMPOSER_ALLOW_SUPERUSER=1
WORKDIR /srv/symfony
COPY . .
RUN export SYMFONY_ENV=prod DATABASE_HOST=null DATABASE_NAME=null DATABASE_USER=null DATABASE_PWD=null &&\
  mkdir -p var web/bundles/fosjsrouting &&\
  composer install --no-progress -o --no-dev > /dev/null 2>&1 ;\
  bin/console fos:js-routing:dump --format=json --target=web/bundles/fosjsrouting/fos_js_routes.json --env=prod


FROM node:9.8.0-stretch AS assets
COPY --from=base /var/lib/apt/lists/* /var/lib/apt/lists/
COPY --from=base /var/cache/apt/archives/* /var/cache/apt/archive/
WORKDIR /tmp/symfony
COPY .babelrc package*.json webpack.config.js ./
RUN mkdir -p app web vendor/friendsofsymfony/jsrouting-bundle/Resources/public/js &&\
  apt-get install -yqq build-essential
COPY ./app/Resources app/Resources
COPY --from=dependencies /srv/symfony/web/bundles web/bundles
COPY --from=dependencies /srv/symfony/vendor/friendsofsymfony/jsrouting-bundle/Resources/public/js/router.js vendor/friendsofsymfony/jsrouting-bundle/Resources/public/js/router.js
RUN npm set progress=false &&\
  npm config set depth 0 &&\
  npm config set loglevel error &&\
  npm install &&\
  export NODE_ENV=production &&\
  npm run -s build


FROM php:7.2-apache
LABEL maintainer="Jaime Leonardo Suncin Cruz <leosuncin@gmail.com>"
ARG TIMEZONE=America/El_Salvador
ARG PHP_MEMORY_LIMIT=512M
ARG PHP_UPLOAD_MAX_FILESIZE=200M
ARG PHP_MAX_FILE_UPLOADS=200
ARG PHP_POST_MAX_SIZE=100M
ENV SYMFONY_ENV=prod
COPY --chown=www-data:www-data . .
COPY --from=dependencies --chown=www-data:www-data /srv/symfony/vendor vendor
COPY --from=assets --chown=www-data:www-data /tmp/symfony/web/assets web/assets
RUN apt-get update &&\
  apt-get install -yqq --no-install-recommends lynx libzip4 zlib1g libxml2 libfreetype6 libjpeg62-turbo libpng16-16 &&\
  rm -rf /var/lib/apt/lists/* /var/cache/apt/archives/*
COPY --from=base /usr/local/lib/php/extensions/no-debug-non-zts-20170718/* /usr/local/lib/php/extensions/no-debug-non-zts-20170718/
COPY --from=base /usr/local/etc/php/conf.d/* /usr/local/etc/php/conf.d/
RUN mv -f docker/000-default.conf /etc/apache2/sites-available/ &&\
  mv docker/apache/check_db.php /usr/local/bin/check_db.php &&\
  mv docker/run.sh /usr/local/bin/run.sh &&\
  chmod +x /usr/local/bin/check_db.php /usr/local/bin/run.sh &&\
  ln -snf /usr/share/zoneinfo/${TIMEZONE} /etc/localtime && echo ${TIMEZONE} > /etc/timezone &&\
  printf "date.timezone = ${TIMEZONE}\nmemory_limit = ${PHP_MEMORY_LIMIT}\nupload_max_filesize = ${PHP_UPLOAD_MAX_FILESIZE}\nmax_file_uploads = ${PHP_MAX_FILE_UPLOADS}\npost_max_size = ${PHP_POST_MAX_SIZE}\ncgi.fix_pathinfo = Off\nshort_open_tag = Off\n" \
  > /usr/local/etc/php/conf.d/php.ini &&\
  a2enmod rewrite &&\
  apachectl configtest
HEALTHCHECK CMD apachectl status || exit 1
CMD ["/usr/local/bin/run.sh"]
