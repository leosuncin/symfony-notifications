#!/usr/bin/env bash
set -eo pipefail

chmod +x bin/*

if [[ ! -f app/config/parameters.yml ]]; then
  printf "parameters:
    database_host: ${DATABASE_HOST}
    database_port: ~
    database_name: ${DATABASE_NAME}
    database_user: ${DATABASE_USER}
    database_password: ${DATABASE_PWD}
    mailer_transport: smtp
    mailer_host: 127.0.0.1
    mailer_user: ~
    mailer_password: ~
    secret: ${SYMFONY_SECRET}
" > app/config/parameters.yml
fi

until check_db.php; do
  >&2 echo "MySQL is unavailable - sleeping"
  sleep 5
done

bin/console cache:warmup --env=prod
bin/console doctrine:migrations:migrate --no-interaction --env=prod

chown -R www-data:www-data var/

source /etc/apache2/envvars
exec apache2 -D FOREGROUND
