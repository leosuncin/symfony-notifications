#!/usr/bin/env php
<?php

$host = getenv('DATABASE_HOST');
$db = getenv('DATABASE_NAME');
$user = getenv('DATABASE_USER');
$pwd = getenv('DATABASE_PWD');

try {
  $connect = new PDO("mysql:host=$host;dbname=$db", $user, $pwd, [PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION]);
  $stament = $connect->query('SELECT 1');
  $count = 0;
  while ($row = $stament->fetch(PDO::FETCH_ASSOC)) {
    $count++;
  }
  if (!$count) die("Could not execute query");
} catch (PDOException $ex) {
  printf("Unable to connect: $ex\n");
  exit(1);
}
