#!/usr/bin/env bash
set -eo pipefail

WORKDIR=${WORKDIR:-/var/www/html}

[[ $WORKDIR != $PWD ]] && cd $WORKDIR

if mount | grep $WORKDIR | grep acl ; then
  if [[ -d $WORKDIR ]]; then
    setfacl -R -m "u:www-data:rX" $WORKDIR
  fi

  if [[ -d $WORKDIR/var ]]; then
    setfacl -R -m "u:www-data:rwX" $WORKDIR/var
    setfacl -dR -m "u:www-data:rwX" $WORKDIR/var
    git clean -dfx var/
  fi
fi

until check_db.php; do
  >&2 echo "MySQL is unavailable - sleeping"
  sleep 5
done

if [[ ! -d vendor ]]; then
  composer install --working-dir=$WORKDIR
fi

chmod +x $WORKDIR/bin/*

bin/console cache:warmup
bin/console doctrine:migrations:migrate --no-interaction
chown -R www-data:www-data var/

source /etc/apache2/envvars
exec apache2 -D FOREGROUND
