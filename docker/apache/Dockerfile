FROM php:7.2-apache

LABEL maintainer="Jaime Leonardo Suncin Cruz <leosuncin@gmail.com>"

ARG TIMEZONE=America/El_Salvador
ARG PHP_MEMORY_LIMIT=512M
ARG PHP_UPLOAD_MAX_FILESIZE=200M
ARG PHP_MAX_FILE_UPLOADS=200
ARG PHP_POST_MAX_SIZE=100M

ENV COMPOSER_ALLOW_SUPERUSER=1

RUN apt-get update && apt-get install -y acl git libxml2-dev libzip-dev elinks libfreetype6-dev libjpeg62-turbo-dev libpng-dev &&\
  ln -snf /usr/share/zoneinfo/${TIMEZONE} /etc/localtime && echo ${TIMEZONE} > /etc/timezone &&\
  docker-php-ext-configure gd --with-freetype-dir=/usr/include/ --with-jpeg-dir=/usr/include/ &&\
  docker-php-ext-configure zip --with-libzip=/usr/include/ &&\
  docker-php-ext-install -j$(nproc) mbstring zip intl pdo pdo_mysql gd &&\
  pecl channel-update pecl.php.net && \
  pecl install xdebug && \
  pecl install apcu && \
  docker-php-ext-enable xdebug apcu &&\
  printf "date.timezone = ${TIMEZONE}\n\
memory_limit = ${PHP_MEMORY_LIMIT}\n\
upload_max_filesize = ${PHP_UPLOAD_MAX_FILESIZE}\n\
max_file_uploads = ${PHP_MAX_FILE_UPLOADS}\n\
post_max_size = ${PHP_POST_MAX_SIZE}\n\
cgi.fix_pathinfo = Off\n\
short_open_tag = Off\n\
error_reporting = E_ALL & ~E_DEPRECATED\n\
display_startup_errors = On\n\
display_errors = On\n\n\
[XDebug]\n\
xdebug.remote_enable = On\n\
xdebug.remote_autostart = On\n\
xdebug.remote_connect_back = On\n\
xdebug.remote_handler = dbgp\n\
xdebug.remote_log = /tmp/xdebug.log\n\
xdebug.idekey = \"VSCODE\"\n\n\
[APC]\n\
apc.enable_cli = On\n\
apc.enabled = On\n\
apc.shm_size = 64M\n" \
  > /usr/local/etc/php/conf.d/php.ini &&\
  curl -sS https://getcomposer.org/installer | php -- --install-dir=/usr/local/bin --filename=composer
COPY symfony.conf /etc/apache2/sites-available/
COPY run.sh /usr/local/bin/run.sh
COPY check_db.php /usr/local/bin/check_db.php
RUN a2enmod info rewrite &&\
  a2dissite 000-default &&\
  a2ensite symfony &&\
  apachectl configtest &&\
  usermod -u 1000 www-data && groupmod -g 1000 www-data &&\
  chmod +x /usr/local/bin/run.sh /usr/local/bin/check_db.php

HEALTHCHECK CMD apachectl status || exit 1

CMD ["/usr/local/bin/run.sh"]
