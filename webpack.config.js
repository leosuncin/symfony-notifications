const MiniCssExtractPlugin = require('mini-css-extract-plugin');
const ReporterPlugin = require('webpack-stylish');
const UglifyJSPlugin = require('uglifyjs-webpack-plugin');
const crypto = require('crypto');
const pkg = require('./package.json');
const webpack = require('webpack');
const {resolve} = require('path');

function md5(data) {
  return crypto
    .createHash('md5')
    .update(data)
    .digest('hex');
}

module.exports = (env = {}) => {
  return {
    entry: ['babel-polyfill', resolve(__dirname, pkg.main)],
    output: {
      path: resolve(__dirname, pkg.config.webpack.outputPath),
      publicPath: pkg.config.webpack.public,
      pathinfo: !env.build,
      filename: 'js/[name].bundle.js',
      chunkFilename: 'js/[name].chunk.js',
    },
    devtool: env.build ? 'source-map' : 'inline-source-map',
    module: {
      rules: [
        {
          test: /\.jsx?$/i,
          exclude: /node_modules|bower_components|jspm_packages/,
          loader: 'babel-loader',
        },
        {
          test: /\.css$/i,
          use: env.build
            ? [
                MiniCssExtractPlugin.loader,
                {
                  loader: 'css-loader',
                  options: {
                    sourceMap: true,
                    minimize: {
                      discardComments: {
                        removeAll: true,
                      },
                      zindex: false,
                    },
                  },
                },
              ]
            : [
                'style-loader',
                {
                  loader: 'css-loader',
                  options: {sourceMap: true},
                },
              ],
        },
        {
          test: /\.scss$/i,
          use: env.build
            ? [
                MiniCssExtractPlugin.loader,
                {
                  loader: 'css-loader',
                  options: {
                    sourceMap: true,
                    minimize: {
                      discardComments: {
                        removeAll: true,
                      },
                      zindex: false,
                      minifyFontValues: false,
                    },
                  },
                },
                'resolve-url-loader',
                {
                  loader: 'sass-loader',
                  options: {
                    sourceMap: true,
                    includePaths: [],
                  },
                },
              ]
            : [
                'style-loader',
                {
                  loader: 'css-loader',
                  options: {sourceMap: true},
                },
                {
                  loader: 'resolve-url-loader',
                  options: {sourceMap: true, keepQuery: true},
                },
                {
                  loader: 'sass-loader',
                  options: {
                    sourceMap: true,
                    includePaths: [],
                  },
                },
              ],
        },
        {
          test: /\.(jpe?g|png|gif)$/i,
          use: [
            {
              loader: 'file-loader',
              options: {
                name: '[name].[ext]',
                outputPath: 'img/',
              },
            },
            {
              loader: 'img-loader',
              options: {
                enabled: env.build,
              },
            },
          ],
        },
        {
          test: /\.(woff2?|[ot]tf|eot|svg)(\?v=\d+(\.\d+\.\d+)?)?$/i,
          loader: 'file-loader',
          include: /fonts?|icons?/i,
          options: {
            outputPath: 'fonts/',
            name: '[name].[ext]',
          },
        },
      ],
    },
    plugins: [
      new webpack.HashedModuleIdsPlugin(),
      new webpack.NamedChunksPlugin(
        chunk =>
          chunk.name
            ? chunk.name
            : md5(chunk.mapModules(m => m.identifier()).join()).slice(0, 10),
      ),
      ...(env.build
        ? [
            new webpack.optimize.ModuleConcatenationPlugin(),
            new webpack.optimize.AggressiveMergingPlugin(),
          ]
        : [new webpack.HotModuleReplacementPlugin()]),
      new webpack.DefinePlugin({
        'process.env.NODE_ENV': process.env.NODE_ENV || '"development"',
      }),
      new webpack.ProvidePlugin({
        $: 'jquery',
        jQuery: 'jquery',
        'window.jQuery': 'jquery',
      }),
      ...(env.build
        ? [
            new MiniCssExtractPlugin({
              filename: 'css/[name].styles.css',
              chunkFilename: 'js/[name].chunk.css',
            }),
            new UglifyJSPlugin({
              sourceMap: true,
              uglifyOptions: {
                mangle: true,
              },
              parallel: true,
            }),
            new ReporterPlugin(),
          ]
        : []),
    ],
    resolve: {
      extensions: ['.js', '.json', '.css', '.scss'],
      modules: [
        resolve(__dirname, 'app/Resources/assets'),
        resolve(__dirname, 'node_modules'),
      ],
      alias: {
        'fos_js_routes.json': resolve(
          __dirname,
          'web/bundles/fosjsrouting/fos_js_routes.json',
        ),
        FOSJsRouting: resolve(
          __dirname,
          'vendor/friendsofsymfony/jsrouting-bundle/Resources/public/js/router.js',
        ),
      },
    },
    stats: env.build ? 'minimal' : 'none',
    ...(env.build
      ? {}
      : {
          devServer: {
            contentBase: resolve(__dirname, pkg.config.webpack.contentBase),
            proxy: {
              '/': process.env.SYMFONY_URL || 'http://localhost:8000/',
            },
          },
        }),
  };
};
