<?php

namespace AppBundle\EventListener;

use AppBundle\Events;
use JMS\Serializer\SerializerInterface;
use Sse\Data;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\EventDispatcher\GenericEvent;

class NotificationSubscriber implements EventSubscriberInterface
{
    private $serializer;

    /**
     * NotificationSubsriber constructor.
     *
     * @param SerializerInterface $serializer
     */
    public function __construct(SerializerInterface $serializer)
    {
        $this->serializer = $serializer;
        $this->storage = new Data('file', ['path' => sys_get_temp_dir() . DIRECTORY_SEPARATOR . 'sse']);
    }

    /**
     * {@inheritdoc}
     */
    public static function getSubscribedEvents()
    {
        return [
            Events::MESSAGE_CREATED => 'onMessageCreated',
            Events::USER_JOINED => 'onUserJoined',
        ];
    }

    /**
     * Listen for new message.
     *
     * @param GenericEvent $event
     */
    public function onMessageCreated(GenericEvent $event)
    {
        $message = $event->getSubject();
        $jsonData = $this->serializer->serialize($message, 'json');
        $this->storage->set(Events::MESSAGE_CREATED, $jsonData);
    }

    /**
     * Listen for joined user.
     *
     * @param GenericEvent $event
     */
    public function onUserJoined(GenericEvent $event)
    {
        $username = $event->getSubject();
        $this->storage->set(Events::USER_JOINED, json_encode([
            'username' => $username,
            'timestamp' => time(),
        ]));
    }
}
