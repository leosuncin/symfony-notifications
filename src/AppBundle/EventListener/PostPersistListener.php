<?php

namespace AppBundle\EventListener;

use AppBundle\Entity\Message;
use AppBundle\Events;
use Doctrine\ORM\Event\LifecycleEventArgs;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;
use Symfony\Component\EventDispatcher\GenericEvent;

class PostPersistListener
{
    private $eventDispatcher;

    /**
     * MessagePostPersistListener.
     *
     * @param EventDispatcherInterface $eventDispatcher
     */
    public function __construct(EventDispatcherInterface $eventDispatcher)
    {
        $this->eventDispatcher = $eventDispatcher;
    }

    /**
     * Listener of post persist message event.
     *
     * @param LifecycleEventArgs $args
     */
    public function postPersist(LifecycleEventArgs $args)
    {
        $entity = $args->getEntity();
        $event = new GenericEvent($entity);

        if ($entity instanceof Message) {
            $this->eventDispatcher->dispatch(Events::MESSAGE_CREATED, $event);
        }
    }
}
