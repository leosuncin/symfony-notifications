<?php

namespace AppBundle;

final class Events
{
    const MESSAGE_CREATED = 'message.created';
    const USER_JOINED = 'user.joined';
}
