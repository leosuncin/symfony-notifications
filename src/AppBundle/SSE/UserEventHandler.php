<?php

namespace AppBundle\SSE;

use AppBundle\Events;
use Sse\Data;
use Sse\Event;

class UserEventHandler implements Event
{
    private $storage;
    private $data;
    private $cache;

    /**
     * UserEventHandler constructor.
     */
    public function __construct()
    {
        $this->storage = new Data('file', ['path' => sys_get_temp_dir() . DIRECTORY_SEPARATOR . 'sse']);
        $this->cache = 0;
    }

    /**
     * {@inheritdoc}
     */
    public function check()
    {
        if (!$this->storage->has(Events::USER_JOINED)) {
            return false;
        }

        $this->data = json_decode($this->storage->get(Events::USER_JOINED));

        if ($this->data->timestamp !== $this->cache) {
            $this->cache = $this->data->timestamp;

            return true;
        }

        return false;
    }

    /**
     * {@inheritdoc}
     */
    public function update()
    {
        return $this->data->username;
    }
}
