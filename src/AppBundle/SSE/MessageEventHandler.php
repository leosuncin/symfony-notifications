<?php

namespace AppBundle\SSE;

use AppBundle\Events;
use Sse\Data;
use Sse\Event;

class MessageEventHandler implements Event
{
    private $storage;
    private $data;
    private $cache;

    /**
     * MessageHandler constructor.
     */
    public function __construct()
    {
        $this->storage = new Data('file', ['path' => sys_get_temp_dir() . DIRECTORY_SEPARATOR . 'sse']);
        $this->cache = time();
    }

    /**
     * {@inheritdoc}
     */
    public function check()
    {
        if (!$this->storage->has(Events::MESSAGE_CREATED)) {
            return false;
        }

        $data = $this->storage->get(Events::MESSAGE_CREATED);
        $message = json_decode($data, true);
        $timestamp = date_create_from_format('Y-m-d\TH:i:sT', $message['sent_at']);

        if ($timestamp && $this->cache < $timestamp->getTimestamp()) {
            $this->cache = $timestamp->getTimestamp();
            $this->data = $data;

            return true;
        }

        return false;
    }

    /**
     * {@inheritdoc}
     */
    public function update()
    {
        return $this->data;
    }
}
