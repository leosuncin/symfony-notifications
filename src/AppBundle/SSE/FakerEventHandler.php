<?php

namespace AppBundle\SSE;

use Faker\Factory;
use Sse\Event;

class FakerEventHandler implements Event
{
    private $faker;

    public function __construct()
    {
        $this->faker = Factory::create();
    }

    /**
     * {@inheritdoc}
     */
    public function check()
    {
        return $this->faker->boolean();
    }

    /**
     * {@inheritdoc}
     */
    public function update()
    {
        $now = new \DateTime();
        $payload = [
            'sender' => $this->faker->userName(),
            'message' => $this->faker->realText(),
            'sentAt' => $now->format('Y-m-d\TH:i:s.z\Z'),
        ];

        return json_encode($payload);
    }
}
