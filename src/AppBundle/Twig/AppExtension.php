<?php

namespace AppBundle\Twig;

use Identicon\Generator\SvgGenerator;
use Identicon\Identicon;

class AppExtension extends \Twig_Extension
{
    public static $units = array(
        'y' => 'year',
        'm' => 'month',
        'd' => 'day',
        'h' => 'hour',
        'i' => 'minute',
        's' => 'second',
    );

    /**
     * {@inheritdoc}
     */
    public function getFilters()
    {
        return array(
            new \Twig_SimpleFilter('identicon', array($this, 'identiconFilter')),
            new \Twig_SimpleFilter('identicon_svg', array($this, 'identiconSVGFilter'), array('is_safe' => ['html'])),
            new \Twig_SimpleFilter('time_ago', array($this, 'timeAgoFilter')),
        );
    }

    /**
     * Generate identicon as base64 PNG from string.
     *
     * @param string       $string
     * @param int          $size
     * @param string|array $color
     *
     * @return string
     */
    public function identiconFilter($string, $size = 64, $color = null)
    {
        $identicon = new Identicon();
        $imageDataUri = $identicon->getImageDataUri($string, $size, $color);

        return $imageDataUri;
    }

    /**
     * Generate identicon as SVG image from string.
     *
     * @param string       $tring
     * @param int          $size
     * @param string|array $color
     *
     * @return string
     */
    public function identiconSVGFilter($tring, $size = 64, $color = null)
    {
        $identicon = new Identicon(new SvgGenerator());

        return $identicon->getImageResource($tring, $size, $color);
    }

    /**
     * How much time has passed until now.
     *
     * @param \DateTime $date
     * @param \DateTime $now
     *
     * @return string
     */
    public function timeAgoFilter(\DateTime $date, \DateTime $now = null)
    {
        if (is_null($now)) {
            $now = new \DateTime();
        }
        $diff = $date->diff($now);
        foreach (self::$units as $attribute => $unit) {
            $count = $diff->$attribute;

            if (0 !== $count) {
                return $this->getPluralizedInterval($count, $diff->invert, $unit);
            }
        }
    }

    /**
     * Get pluralized interval.
     *
     * @param int    $count
     * @param bool   $invert
     * @param string $unit
     *
     * @return string
     */
    private function getPluralizedInterval($count, $invert, $unit)
    {
        if (1 !== $count) {
            $unit .= 's';
        }

        return $invert ? "in $count $unit" : "$count $unit ago";
    }
}
