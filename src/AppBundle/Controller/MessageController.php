<?php

namespace AppBundle\Controller;

use AppBundle\Controller\BaseController as Controller;
use AppBundle\Entity\Message;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

/**
 * Message controller.
 *
 * @Route("message", options={"expose"=true})
 */
class MessageController extends Controller
{
    /**
     * Lists all message entities.
     *
     * @param Request $request
     *
     * @return Response
     *
     * @Route("", name="message_index")
     * @Method("GET")
     */
    public function indexAction(Request $request)
    {
        $limit = $request->query->get('limit', 5);
        $page = $request->query->get('page', 1);

        $entityManager = $this->getDoctrine()->getManager();
        $repository = $entityManager->getRepository(Message::class);
        $dql = $repository->createQueryBuilder('p');

        $messages = $dql->getQuery()
                    ->setFirstResult($limit * ($page - 1))
                    ->setMaxResults($limit)
                    ->getResult();

        return $this->reply('message/index.html.twig', array(
            'messages' => $messages,
        ));
    }

    /**
     * Creates a new message.
     *
     * @param Request $request
     *
     * @return Response
     *
     * @Route("", name="message_create")
     * @Method("POST")
     */
    public function createAction(Request $request)
    {
        $isJSON = $this->isJsonRequest($request);
        $message = new Message();
        $form = $this->createForm(
            'AppBundle\Form\MessageType',
            $message,
            array(
                'action' => $this->generateUrl('message_create'),
                'method' => 'POST',
                'csrf_protection' => !$isJSON,
            )
        );

        if ($isJSON) {
            $data = json_decode($request->getContent(), true);
            unset($data['sentAt']);
            $form->submit($data);
        }
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($message);
            $entityManager->flush();

            return $this->reply('message/show.html.twig', array('message' => $message), 201);
        }

        return $this->createValidationErrorResponse($form);
    }

    /**
     * Shows a new message entity form.
     *
     * @return Response
     *
     * @Route("/create", name="message_new")
     * @Method("GET")
     */
    public function newAction()
    {
        $message = new Message();
        $form = $this->createForm(
            'AppBundle\Form\MessageType',
            $message,
            array(
                'action' => $this->generateUrl('message_create'),
                'method' => 'POST',
            )
        );

        return $this->render('message/new.html.twig', array(
            'message' => $message,
            'form' => $form->createView(),
        ));
    }

    /**
     * Finds and displays a message entity.
     *
     * @param Message $message
     *
     * @return Response
     *
     * @Route("/{id}", name="message_show", requirements={"id"="\d+"})
     * @Method("GET")
     */
    public function showAction(Message $message)
    {
        return $this->reply('message/show.html.twig', array('message' => $message));
    }

    /**
     * Update a message.
     *
     * @param Request $request
     * @param Message $message
     *
     * @return Response
     *
     * @Route("/{id}", name="message_update", requirements={"id"="\d+"})
     * @Method("PUT")
     */
    public function updateAction(Request $request, Message $message)
    {
        $isJSON = $this->isJsonRequest($request);
        $editForm = $this->createForm(
            'AppBundle\Form\MessageType',
            $message,
            array(
                'action' => $this->generateUrl(
                    'message_update',
                    array('id' => $message->getId())
                ),
                'method' => 'PUT',
                'csrf_protection' => !$isJSON,
            )
        );

        if ($isJSON) {
            $data = json_decode($request->getContent(), true);
            unset($data['sentAt']);
            $editForm->submit($data);
        }
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->reply('message/show.html.twig', array('message' => $message));
        }

        return $this->createValidationErrorResponse($editForm);
    }

    /**
     * Displays a form to edit an existing message entity.
     *
     * @param Request $request
     * @param Message $message
     *
     * @return Response
     *
     * @Route("/{id}/edit", name="message_edit", requirements={"id"="\d+"})
     * @Method("GET")
     */
    public function editAction(Request $request, Message $message)
    {
        $editForm = $this->createForm(
            'AppBundle\Form\MessageType',
            $message,
            array(
                'action' => $this->generateUrl(
                    'message_update',
                    array('id' => $message->getId())
                ),
                'method' => 'PUT',
            )
        );
        $editForm->handleRequest($request);

        return $this->render('message/edit.html.twig', array(
            'message' => $message,
            'edit_form' => $editForm->createView(),
        ));
    }

    /**
     * Deletes a message entity.
     *
     * @param Message $message
     *
     * @return Response
     *
     * @Route("/{id}", name="message_delete", requirements={"id"="\d+"})
     * @Method("DELETE")
     */
    public function deleteAction(Message $message)
    {
        $entityManager = $this->getDoctrine()->getManager();
        $entityManager->remove($message);
        $entityManager->flush();

        return new Response('', 204);
    }
}
