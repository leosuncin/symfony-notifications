<?php

namespace AppBundle\Controller;

use AppBundle\Events;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;
use Symfony\Component\EventDispatcher\GenericEvent;
use Symfony\Component\HttpFoundation\Request;

class DefaultController extends Controller
{
    /**
     * Show the homepage.
     *
     * @Route("/", name="homepage", options={"expose"=true})
     */
    public function indexAction(Request $request)
    {
        return $this->render('default/index.html.twig');
    }

    /**
     * Show the chat page.
     *
     * @Route("/chat", name="chatpage", options={"expose"=true})
     */
    public function chatAction(Request $request, EventDispatcherInterface $eventDispatcher)
    {
        $username = $request->get('username');

        if (empty($username)) {
            $this->addFlash('warning', 'Your username is required');

            return $this->redirectToRoute('homepage');
        }

        $event = new GenericEvent($username);
        $eventDispatcher->dispatch(Events::USER_JOINED, $event);

        return $this->render('default/chat.html.twig', ['username' => $username]);
    }
}
