<?php

namespace AppBundle\Controller;

use AppBundle\Controller\BaseController as Controller;
use AppBundle\SSE\MessageEventHandler;
use AppBundle\SSE\UserEventHandler;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sse\SSE;

/**
 * @Route("/notification", options={"expose"=true})
 */
class NotificationController extends Controller
{
    /**
     * Stream notifications.
     *
     * @Route("/sse", name="notification_sse")
     */
    public function sseAction()
    {
        $sse = new SSE();
        $sse->addEventListener('message', new MessageEventHandler());
        $sse->addEventListener('user', new UserEventHandler());

        return $sse->createResponse();
    }
}
